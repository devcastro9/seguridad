﻿using Microsoft.Extensions.DependencyInjection;
using Seguridad.Services;
using System;
using System.Windows.Forms;

namespace Seguridad
{
    public partial class FormLogin : Form
    {
        private readonly IServiceProvider _serviceProvider;
        private IUsuarioData _user;
        public FormLogin(IServiceProvider serviceProvider, IUsuarioData usuario)
        {
            InitializeComponent();
            _serviceProvider = serviceProvider;
            _user = usuario;
        }

        private void BtnSalir_Click(object sender, System.EventArgs e)
        {
            Application.Exit();
        }

        private async void BtnLogin_Click(object sender, System.EventArgs e)
        {
            bool auth = await _user.Login(TxtUsuario.Text, txtPassword.Text);
            if (auth)
            {
                FormMain frmMain = _serviceProvider.GetRequiredService<FormMain>();
                frmMain.Show();
                Hide();
            }
            else
            {
                MessageBox.Show("Usuario o password incorrecto.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtPassword.Clear();
                TxtUsuario.Focus();
            }
        }

        private void FormLogin_Load(object sender, EventArgs e)
        {
            TxtUsuario.Focus();
        }
    }
}
