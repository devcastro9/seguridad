﻿using Microsoft.EntityFrameworkCore;
using Seguridad.Models;

namespace Seguridad.Data;

public partial class AppDbContext : DbContext
{
    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

    public virtual DbSet<GcControl> GcControles { get; set; }

    public virtual DbSet<GcFormulario> GcFormularios { get; set; }

    public virtual DbSet<GcRight> GcRights { get; set; }

    public virtual DbSet<GcRole> GcRoles { get; set; }

    public virtual DbSet<GcUsuario> GcUsuarios { get; set; }

    public virtual DbSet<GcUsuariosRole> GcUsuariosRoles { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // Metodo base
        base.OnModelCreating(modelBuilder);
        // Api fluent
        modelBuilder.Entity<GcControl>(entity =>
        {
            entity.HasKey(e => e.CtrlId).HasName("PK_gc_ctrl");

            entity.HasOne(d => d.Form).WithMany(p => p.GcControles).HasConstraintName("FK_gc_controles_gc_formularios");
        });

        modelBuilder.Entity<GcFormulario>(entity =>
        {
            entity.HasKey(e => e.FormId).HasName("PK_gc_forms");

            entity.Property(e => e.FechaSubido).HasDefaultValueSql("(getdate())");
        });

        modelBuilder.Entity<GcRight>(entity =>
        {
            entity.HasOne(d => d.Ctrl).WithMany(p => p.GcRights).HasConstraintName("FK_gc_right_gc_controles");

            entity.HasOne(d => d.IdRoleNavigation).WithMany(p => p.GcRights).HasConstraintName("FK_gc_right_gc_roles");
        });

        modelBuilder.Entity<GcUsuario>(entity =>
        {
            entity.Property(e => e.DeptoCodigo).HasDefaultValueSql("((2))");
            entity.Property(e => e.EstadoCodigo)
                .HasDefaultValueSql("('REG')")
                .IsFixedLength();
            entity.Property(e => e.ValidarVersion).HasDefaultValueSql("((1))");
        });

        modelBuilder.Entity<GcUsuariosRole>(entity =>
        {
            entity.Property(e => e.Fecha).HasDefaultValueSql("(getdate())");

            entity.HasOne(d => d.Role).WithMany(p => p.GcUsuariosRoles)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_gc_usuarios_roles_gc_roles");

            entity.HasOne(d => d.UsrCodigo).WithMany(p => p.GcUsuariosRoles)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_gc_usuarios_roles_gc_usuarios");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
