﻿using Microsoft.EntityFrameworkCore;
using Seguridad.Data;
using Seguridad.Models;
using Seguridad.Utility;
using System.Linq;
using System.Threading.Tasks;

namespace Seguridad.Services
{
    public class UsuarioData : IUsuarioData
    {
        private readonly AppDbContext _db;
        public string? Usr_codigo { get; set; }
        public string? Usr_nombres { get; set; }
        public string? Usr_primer_apellido { get; set; }
        public string? Usr_segundo_apellido { get; set; }
        public UsuarioData(AppDbContext dbContext)
        {
            _db = dbContext;
        }
        public async Task<bool> Login(string user, string pass)
        {
            string passDecript = Util.Decrypt(pass);
            GcUsuario? result = await _db.GcUsuarios.AsNoTracking().Where(m => m.UsrCodigo == user && m.UsrClave == passDecript).FirstOrDefaultAsync();
            if (result == null)
            {
                return false;
            }
            Usr_codigo = result.UsrCodigo;
            Usr_nombres = result.UsrNombres;
            Usr_primer_apellido = result.UsrPrimerApellido;
            Usr_segundo_apellido = result.UsrSegundoApellido;
            return true;
        }
    }
}
