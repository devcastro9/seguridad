﻿using System.Threading.Tasks;

namespace Seguridad.Services
{
    public interface IUsuarioData
    {
        public string? Usr_codigo { get; set; }
        public string? Usr_nombres { get; set; }
        public string? Usr_primer_apellido { get; set; }
        public string? Usr_segundo_apellido { get; set; }
        public Task<bool> Login(string user, string pass);
    }
}
