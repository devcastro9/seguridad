﻿using System.Linq;
using System.Text;

namespace Seguridad.Utility
{
    public static class Util
    {
        public static string Decrypt(string pass) => Encoding.ASCII.GetBytes(pass).Select(x => char.ConvertFromUtf32(x + 5)).Aggregate("", (ac, x) => ac + x);
    }
}
