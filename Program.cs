using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Seguridad.Data;
using Seguridad.Services;
using System;
using System.Windows.Forms;

namespace Seguridad
{
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // To customize application configuration such as set high DPI settings or default font,
            // see https://aka.ms/applicationconfiguration.
            ApplicationConfiguration.Initialize();
            IHostBuilder builder = Host.CreateDefaultBuilder().ConfigureServices((context, services) =>
            {
                services.AddDbContext<AppDbContext>(options =>
                {
                    options.UseSqlServer(context.Configuration.GetConnectionString("Dbase"));
                    //options.EnableSensitiveDataLogging();
                });
                services.AddSingleton<FormLogin>();
                services.AddTransient<FormMain>();
                services.AddTransient<FormAsignaRoles>();
                services.AddTransient<FormRoles>();
                services.AddTransient<FormUsuarios>();
                services.AddTransient<FormUsuarioEdit>();
                services.AddScoped<IUsuarioData, UsuarioData>();
            });
            IHost host = builder.Build();
            using IServiceScope serviceScope = host.Services.CreateScope();
            IServiceProvider servicios = serviceScope.ServiceProvider;
            Application.Run(servicios.GetRequiredService<FormLogin>());
        }
    }
}