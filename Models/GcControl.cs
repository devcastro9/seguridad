﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Seguridad.Models;

[Table("gc_controles")]
public partial class GcControl
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int CtrlId { get; set; }

    public int? FormId { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? NombreControl { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? TipoControl { get; set; }

    [Column("fecha_modificado", TypeName = "datetime")]
    public DateTime? FechaModificado { get; set; }

    [ForeignKey("FormId")]
    [InverseProperty("GcControles")]
    public virtual GcFormulario? Form { get; set; }

    [InverseProperty("Ctrl")]
    public virtual ICollection<GcRight> GcRights { get; } = new List<GcRight>();
}
