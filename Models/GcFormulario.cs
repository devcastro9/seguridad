﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Seguridad.Models;

[Table("gc_formularios")]
public partial class GcFormulario
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int FormId { get; set; }

    [StringLength(150)]
    [Unicode(false)]
    public string? ArchivoForm { get; set; }

    [StringLength(50)]
    [Unicode(false)]
    public string? NombreForm { get; set; }

    [StringLength(100)]
    [Unicode(false)]
    public string? Caption { get; set; }

    [Column("fecha_subido", TypeName = "datetime")]
    public DateTime? FechaSubido { get; set; }

    [InverseProperty("Form")]
    public virtual ICollection<GcControl> GcControles { get; } = new List<GcControl>();
}
