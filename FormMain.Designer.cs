﻿namespace Seguridad
{
    partial class FormMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PanelMenu = new System.Windows.Forms.Panel();
            this.BtnRoles = new FontAwesome.Sharp.IconButton();
            this.BtnAsignaRol = new FontAwesome.Sharp.IconButton();
            this.BtnUsuarios = new FontAwesome.Sharp.IconButton();
            this.PnlTitulo = new System.Windows.Forms.Panel();
            this.BtnSalir = new FontAwesome.Sharp.IconButton();
            this.PnlForms = new System.Windows.Forms.Panel();
            this.LblMain = new System.Windows.Forms.Label();
            this.PanelMenu.SuspendLayout();
            this.PnlTitulo.SuspendLayout();
            this.PnlForms.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelMenu
            // 
            this.PanelMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(38)))), ((int)(((byte)(70)))));
            this.PanelMenu.Controls.Add(this.BtnRoles);
            this.PanelMenu.Controls.Add(this.BtnAsignaRol);
            this.PanelMenu.Controls.Add(this.BtnUsuarios);
            this.PanelMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelMenu.Location = new System.Drawing.Point(0, 0);
            this.PanelMenu.Name = "PanelMenu";
            this.PanelMenu.Size = new System.Drawing.Size(250, 800);
            this.PanelMenu.TabIndex = 0;
            // 
            // BtnRoles
            // 
            this.BtnRoles.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(38)))), ((int)(((byte)(70)))));
            this.BtnRoles.FlatAppearance.BorderSize = 0;
            this.BtnRoles.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnRoles.Font = new System.Drawing.Font("Segoe UI Semibold", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.BtnRoles.ForeColor = System.Drawing.Color.White;
            this.BtnRoles.IconChar = FontAwesome.Sharp.IconChar.TowerObservation;
            this.BtnRoles.IconColor = System.Drawing.Color.White;
            this.BtnRoles.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.BtnRoles.Location = new System.Drawing.Point(3, 340);
            this.BtnRoles.Name = "BtnRoles";
            this.BtnRoles.Size = new System.Drawing.Size(250, 64);
            this.BtnRoles.TabIndex = 2;
            this.BtnRoles.Text = "Roles";
            this.BtnRoles.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnRoles.UseVisualStyleBackColor = false;
            this.BtnRoles.Click += new System.EventHandler(this.BtnRoles_Click);
            // 
            // BtnAsignaRol
            // 
            this.BtnAsignaRol.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(38)))), ((int)(((byte)(70)))));
            this.BtnAsignaRol.FlatAppearance.BorderSize = 0;
            this.BtnAsignaRol.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnAsignaRol.Font = new System.Drawing.Font("Segoe UI Semibold", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.BtnAsignaRol.ForeColor = System.Drawing.Color.White;
            this.BtnAsignaRol.IconChar = FontAwesome.Sharp.IconChar.Users;
            this.BtnAsignaRol.IconColor = System.Drawing.Color.White;
            this.BtnAsignaRol.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.BtnAsignaRol.Location = new System.Drawing.Point(1, 248);
            this.BtnAsignaRol.Name = "BtnAsignaRol";
            this.BtnAsignaRol.Size = new System.Drawing.Size(250, 64);
            this.BtnAsignaRol.TabIndex = 1;
            this.BtnAsignaRol.Text = "Asignacion de Rol";
            this.BtnAsignaRol.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnAsignaRol.UseVisualStyleBackColor = false;
            this.BtnAsignaRol.Click += new System.EventHandler(this.BtnAsignaRol_Click);
            // 
            // BtnUsuarios
            // 
            this.BtnUsuarios.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(38)))), ((int)(((byte)(70)))));
            this.BtnUsuarios.FlatAppearance.BorderSize = 0;
            this.BtnUsuarios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnUsuarios.Font = new System.Drawing.Font("Segoe UI Semibold", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.BtnUsuarios.ForeColor = System.Drawing.Color.White;
            this.BtnUsuarios.IconChar = FontAwesome.Sharp.IconChar.UserAlt;
            this.BtnUsuarios.IconColor = System.Drawing.Color.White;
            this.BtnUsuarios.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.BtnUsuarios.Location = new System.Drawing.Point(0, 160);
            this.BtnUsuarios.Name = "BtnUsuarios";
            this.BtnUsuarios.Size = new System.Drawing.Size(250, 64);
            this.BtnUsuarios.TabIndex = 0;
            this.BtnUsuarios.Text = "Usuarios";
            this.BtnUsuarios.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnUsuarios.UseVisualStyleBackColor = false;
            this.BtnUsuarios.Click += new System.EventHandler(this.BtnUsuarios_Click);
            // 
            // PnlTitulo
            // 
            this.PnlTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(38)))), ((int)(((byte)(70)))));
            this.PnlTitulo.Controls.Add(this.BtnSalir);
            this.PnlTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.PnlTitulo.Location = new System.Drawing.Point(250, 0);
            this.PnlTitulo.Name = "PnlTitulo";
            this.PnlTitulo.Size = new System.Drawing.Size(1250, 50);
            this.PnlTitulo.TabIndex = 1;
            // 
            // BtnSalir
            // 
            this.BtnSalir.FlatAppearance.BorderSize = 0;
            this.BtnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSalir.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.BtnSalir.IconChar = FontAwesome.Sharp.IconChar.X;
            this.BtnSalir.IconColor = System.Drawing.Color.White;
            this.BtnSalir.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.BtnSalir.IconSize = 30;
            this.BtnSalir.Location = new System.Drawing.Point(1199, 5);
            this.BtnSalir.Name = "BtnSalir";
            this.BtnSalir.Size = new System.Drawing.Size(49, 41);
            this.BtnSalir.TabIndex = 0;
            this.BtnSalir.UseVisualStyleBackColor = false;
            this.BtnSalir.Click += new System.EventHandler(this.BtnSalir_Click);
            // 
            // PnlForms
            // 
            this.PnlForms.Controls.Add(this.LblMain);
            this.PnlForms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PnlForms.Location = new System.Drawing.Point(250, 50);
            this.PnlForms.Name = "PnlForms";
            this.PnlForms.Size = new System.Drawing.Size(1250, 750);
            this.PnlForms.TabIndex = 2;
            // 
            // LblMain
            // 
            this.LblMain.AutoSize = true;
            this.LblMain.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LblMain.Font = new System.Drawing.Font("Segoe UI Symbol", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.LblMain.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(49)))), ((int)(((byte)(104)))));
            this.LblMain.Location = new System.Drawing.Point(385, 322);
            this.LblMain.Name = "LblMain";
            this.LblMain.Size = new System.Drawing.Size(480, 106);
            this.LblMain.TabIndex = 0;
            this.LblMain.Text = "SEGURIDAD";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1500, 800);
            this.Controls.Add(this.PnlForms);
            this.Controls.Add(this.PnlTitulo);
            this.Controls.Add(this.PanelMenu);
            this.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormMain";
            this.PanelMenu.ResumeLayout(false);
            this.PnlTitulo.ResumeLayout(false);
            this.PnlForms.ResumeLayout(false);
            this.PnlForms.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelMenu;
        private FontAwesome.Sharp.IconButton BtnUsuarios;
        private FontAwesome.Sharp.IconButton BtnRoles;
        private FontAwesome.Sharp.IconButton BtnAsignaRol;
        private System.Windows.Forms.Panel PnlTitulo;
        private FontAwesome.Sharp.IconButton BtnSalir;
        private System.Windows.Forms.Panel PnlForms;
        private System.Windows.Forms.Label LblMain;
    }
}