using Microsoft.Extensions.DependencyInjection;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Seguridad
{
    public partial class FormMain : Form
    {
        private readonly Color Activo = Color.FromArgb(46, 49, 104);
        private readonly Color Inactivo = Color.FromArgb(30, 38, 70);
        private readonly IServiceProvider _serviceProvider;
        public FormMain(IServiceProvider serviceProvider)
        {
            InitializeComponent();
            _serviceProvider = serviceProvider;
        }

        private void BtnSalir_Click(object sender, System.EventArgs e)
        {
            Application.Exit();
        }

        private void BtnUsuarios_Click(object sender, System.EventArgs e)
        {
            OpenChildFrm(_serviceProvider.GetRequiredService<FormUsuarios>());
            BtnUsuarios.BackColor = Activo;
            BtnAsignaRol.BackColor = Inactivo;
            BtnRoles.BackColor = Inactivo;
        }

        private void BtnAsignaRol_Click(object sender, System.EventArgs e)
        {
            OpenChildFrm(_serviceProvider.GetRequiredService<FormAsignaRoles>());
            BtnUsuarios.BackColor = Inactivo;
            BtnAsignaRol.BackColor = Activo;
            BtnRoles.BackColor = Inactivo;
        }

        private void BtnRoles_Click(object sender, System.EventArgs e)
        {
            OpenChildFrm(_serviceProvider.GetRequiredService<FormRoles>());
            BtnUsuarios.BackColor = Inactivo;
            BtnAsignaRol.BackColor = Inactivo;
            BtnRoles.BackColor = Activo;
        }
        private Form? ActiveFrm = null;
        private void OpenChildFrm(Form ChildForm)
        {
            ActiveFrm?.Close();
            ActiveFrm = ChildForm;
            ChildForm.TopLevel = false;
            ChildForm.FormBorderStyle = FormBorderStyle.None;
            ChildForm.Dock = DockStyle.Fill;
            PnlForms.Controls.Add(ChildForm);
            PnlForms.Tag = ChildForm;
            ChildForm.BringToFront();
            ChildForm.Show();
        }
    }
}